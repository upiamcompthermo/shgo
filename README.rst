.. image:: https://travis-ci.org/Stefan-Endres/shgo.svg?branch=master
    :target: https://travis-ci.org/Stefan-Endres/shgo
.. image:: https://coveralls.io/repos/github/Stefan-Endres/shgo/badge.png?branch=master
    :target: https://coveralls.io/github/Stefan-Endres/shgo?branch=master

NOTE:
This repository has been permenantly moved to https://github.com/Stefan-Endres/shgo. 

The tag jogo_Sobol is kept here for archival purposes for the paper "A simplicial homology algorithm for Lipschitz optimisation" published in the Journal of Global Optimization. 